<?php
include_once 'is_login.php';
$title="人工处理";
include_once 'top.php';
$strSql ="select a.money,a.status,a.way,a.u_id,a.time,a.id,u.uid,u.name from c_apply a join c_user u on a.u_id=u.uid where a.id='".$_GET['t']."' and status=0 order by money desc";
$ti=$pdo -> query($strSql, $queryMode = 'Row', $debug = false);
?>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 申请中心 <span class="c-gray en">&gt;</span> 拆分订单 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="pd-20">

    <div class="text-c">
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
     总金额:<span id="sum"><?php echo $ti['money']?></span><br><br>
    <input type="text" class="input-text" style="width:40%"  name="" placeholder="拆分订单金额1" id="one" onchange="$('#two').val($('#sum').html()-$(this).val())"><br><br>
    <input type="text" class="input-text" style="width:40%" name="" placeholder="拆分订单金额2" id="two">
    <br><input type="submit" class="btn btn-success" value="拆分" onclick="return fun(<?php echo $ti['id']?>,<?php echo $ti['uid']?>);" style="margin-left: 10px;margin-top:40px">

    <input type="submit" value="清除" class="btn btn-success" onclick="return qing();" style="margin-left: 10px;margin-top:40px">

    </div>
    <div id="pageNav" class="pageNav"></div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    window.onload = (function(){
        // optional set
        pageNav.pre="&lt;上一页";
        pageNav.next="下一页&gt;";
        // p,当前页码,pn,总页面
        pageNav.fn = function(p,pn){$("#pageinfo").text("当前页:"+p+" 总页: "+pn);};
        //重写分页状态,跳到第三页,总页33页
        pageNav.go(1,13);
    });
    function fun(id,uid){
        var sum=parseInt($("#sum").text());
        var one=$("#one").val();
        var two=$("#two").val();
        if(parseInt(one)<0 || parseInt(two)<0){
            alert("提现金额不能为负");
            return false;
        }
        var there=parseInt(one)+parseInt(two);
        if(there!=sum){
            alert("总和不等于原金额");
            return false;
        }else{
            $.get("function.php",{id:id,uid:uid,one:one,two:two},function(data){
                    /*console.log(data)*/
                   alert('拆分成功..');window.location.href='apply.php';
            })
        }

    }
    function qing(){

        $("#one").val('');
        $("#two").val('');
    }

</script>
</body>
</html>

