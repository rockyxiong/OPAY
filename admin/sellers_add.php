<?php
include_once 'is_login.php';
$title="会员修改";
include_once 'top.php';
if(@$_POST['sub']){
  if($_FILES['img']['tmp_name'] != ""){
    $img=$pdo->up($_FILES['img']);
  }else{
    $img=$ress['img'];
  }
    $data=array(
    'name'=>$_POST['name'],
    'type'=>$_POST['type'],
    'address'=>$_POST['address'],
    'tel'=>$_POST['tel'],
    'pwd'=>$_POST['pwd'],
    'img'=>$img,
    'status'=>$_POST['status'],
    'jfbl'=>$_POST['jfbl'],
    'dksx'=>$_POST['dksx'],
  );
  $result = $pdo -> insert('w_sellers', $data, $debug = false);
  $pdo->jump('添加成功','sellers_list.php');
}

$array=[
  '景区','酒店','餐饮','保健','娱乐','服装','鞋包','酒类','饮料'
];

$sheng = ['安徽','北京','重庆','福建','甘肃','广东','广西','贵州','海南','河北','黑龙江','河南','香港','湖北','湖南','江苏','江西','吉林','辽宁','澳门','内蒙古','宁夏','青海','山东','上海','山西','陕西','四川','台湾','天津','新疆','西藏','云南','浙江','海外'];
?>
<body>
<article class="page-container">
  <form action="" method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>姓名：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="username" name="name">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>登陆账号：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="username" name="tel">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3">头像</label>
      <div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
        <a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
        <input type="file"  name="img" class="input-file">
        </span> </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>登陆密码：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="pwd" name="pwd">
      </div>
    </div>

    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>赠送积分（%）：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="jfbl" name="jfbl">
      </div>
    </div>

    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>抵扣上限（%）：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="dksx" name="dksx">
      </div>
    </div>
    
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>所属类型:</label>
      <div class="formControls col-xs-8 col-sm-9">
        <select name="type">
          <?php
          foreach ($array as $v) {
          ?>
          <option value="<?php echo $v?>"><?php echo $v?></option>
          <?php }?>
        </select>
      </div>
    </div>
       <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>请选择商家城市:</label>
      <div class="formControls col-xs-8 col-sm-9">
        <select name="address">
          <?php
          foreach ($sheng as $v){
          ?>
          <option value="<?php echo $v?>"><?php echo $v?></option>
          <?php }?>
        </select>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>审核状态:</label>
      <div class="formControls col-xs-8 col-sm-9">
        <select name="status">
          <option value="0">待审</option>
          <option value="1">审核通过</option>
        </select>
      </div>
    </div>
  
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
        <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" name="sub">
      </div>
    </div>
    
  </form>
  </article>




<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>

<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>