<?php
include_once 'is_login.php';
$title="会员修改";
include_once 'top.php';
if(@$_POST['sub']){

	$strSqls = 'select * from w_sellers where id='.$_POST['hid'];
	$ress = $pdo -> query($strSqls, $queryMode = 'Row', $debug = false);
	if($_FILES['img']['tmp_name'] != ""){
		$img=$pdo->up($_FILES['img']);
	}else{
		$img=$ress['img'];
	}
	if($_POST['pwd']==""){
		
		$data=array(
		'address2'=>$_POST['address2'],
		'name'=>$_POST['name'],
		'tel'=>$_POST['tel'],
		'pwd'=>$ress['pwd'],
		'img'=>$img,
		'card'=>$_POST['card'],
		'status'=>$_POST['status'],
		'zfb'=>$_POST['zfb'],
		'yhk'=>$_POST['yhk'],
		'zfpwd'=>$_POST['zfpwd'],
		'jfbl'=>$_POST['jfbl'],
		'dksx'=>$_POST['dksx'],
	);
	}else{
		$data=array(
		'address2'=>$_POST['address2'],
		'name'=>$_POST['name'],
		'tel'=>$_POST['tel'],
		'pwd'=>$_POST['pwd'],
		'img'=>$img,
		'card'=>$_POST['card'],
		'status'=>$_POST['status'],
		'zfb'=>$_POST['zfb'],
		'yhk'=>$_POST['yhk'],
		'zfpwd'=>$_POST['zfpwd'],
		'jfbl'=>$_POST['jfbl'],
		'dksx'=>$_POST['dksx'],
	);
	}
	$result = $pdo -> update('w_sellers',$data,'id ='.$_POST['hid'], $debug = false);
	//zidong('user-list.php','修改成功',1);die;
   $pdo->jump('修改成功','sellers_list.php');
}else{
	$id=@$_GET['id'];
	$strSql = 'select * from w_sellers where id ='.$id;
	$arr = $pdo -> query($strSql, $queryMode = 'Row', $debug = false);
}
?>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>姓名：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['name']?>" placeholder="" id="username" name="name">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>登陆账号：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['tel']?>" placeholder="" id="username" name="tel">
			</div>
		</div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>当前余额:</label>
            <div class="formControls col-xs-8 col-sm-9"><?php echo $arr['money']?>
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>赠送积分（%）:</label>
            <div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['jfbl']?>" placeholder="" id="jfbl" name="jfbl">
			</div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>抵扣上限（%）:</label>
            <div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['dksx']?>" placeholder="" id="dksx" name="dksx">
			</div>
        </div>

		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">头像</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
				
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
				<input type="file"  name="img" class="input-file">
				</span> </div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>登陆密码：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="" placeholder="" id="pwd" name="pwd">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>银行卡:</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['yhk']?>" placeholder=""  name="yhk">
			</div>
		</div>

        <div class="row cl" >
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>身份证:</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<?php echo $arr['card']?>" placeholder=""  name="card">
            </div>
        </div>
        <div class="row cl" >
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>地址:</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="<?php echo $arr['address2']?>" placeholder=""  name="address2">
            </div>
        </div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>支付宝账号:</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['zfb']?>" placeholder=""  name="zfb">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>支付密码:</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $arr['zfpwd']?>" placeholder=""  name="zfpwd">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>审核状态:</label>
			<div class="formControls col-xs-8 col-sm-9">
				<select name="status">
					<option value="0" <?php if($arr['status']==0){ echo "selected='selected'";}?>>待审</option>
					<option value="1" <?php if($arr['status']==1){ echo "selected='selected'";}?>>审核通过</option>
				</select>
			</div>
		</div>
		<input type="hidden" name="hid" value="<?php echo $arr['id']?>">
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" name="sub">
			</div>
		</div>
		
	</form>
	</article>




<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>

<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>