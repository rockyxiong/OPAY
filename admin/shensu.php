<?php 
include_once 'is_login.php';
include_once 'top.php';

if($sta=@$_GET['status'] && $id=@$_GET['id'] ){
  $result = $pdo -> update('c_complain',array('status'=>$sta),'id ='.$id, $debug = false);
  die;
}
$strSql = "select * from `c_complain`  order by status,ts_time desc";
$list = $pdo -> query($strSql, $queryMode = 'All', $debug = false);
$status=[0=>"未处理",1=>"已处理"];



$strSql1 = "select * from c_user";
$res = $pdo -> query($strSql1, $queryMode = 'All', $debug = false);

foreach($res as $v){
  $arr[$v['uid']]['name']=$v['name'];
  $arr[$v['uid']]['tel']=$v['tel'];
}
?>

<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 申诉中心 <span class="c-gray en">&gt;</span> 申诉列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <div class="text-c"> 

  
  </div>
  <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> </span> <span class="r">共有数据：<strong><?php echo count($list)??0?></strong> 条</span> </div>
  <div class="mt-20">
  <table class="table table-border table-bordered table-hover table-bg table-sort">
    <thead>
      <tr class="text-c">
        
        <th width="20">ID</th>
        <th width="100">申诉者</th>
        <th width="40">申诉标题</th>
        <th width="90">联系方式</th>
         <th width="90">申诉内容</th>
        <th width="90">申诉图片1</th>
        <th width="90">申诉图片2</th>
        <th width="90">申诉状态</th>
        <th width="90">时间</th>
        <th width="100">操作</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($list as $v) {
    ?>
      <tr class="text-c">
       
        <td><?php echo $i++;?></td>
        <td><?php echo $arr[$v['uid']]['name'];?></td>
        <td><?php echo $v['title'];?></td>
          <td>123</td>
         <td><?php echo $v['con'];?></td>
           <td><img src="../<?php echo $v['pic1'];?>" width="100px"></td>
           <td><img src="../<?php echo $v['pic2'];?>" width="100px"></td>
            <td><?php echo $status[$v['status']];?></td>
        <td><?php echo date('Y-m-d',$v['ts_time']);?></td>
        <td class="td-manage">
          <select id="sta" bgid="<?php echo $v['id']?>" >
             <option value="0" <?php if($v['status']==0){echo "selected='selected'";}?>>未处理</option>
             <option value="1" <?php if($v['status']==1){echo "selected='selected'";}?>>已处理</option>
          </select>
         </td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
$(function(){
  $('.table-sort').dataTable({
    "aaSorting": [[ 1, "desc" ]],//默认第几个排序
    "bStateSave": true,//状态保存
    "aoColumnDefs": [
      //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
      {"orderable":false,"aTargets":[0]}// 制定列不参与排序
    ]
  });
  
});


function datadel(){

  if(confirm("确定要删除所选项目？")) { 
    var checkedList = new Array(); 
    $("input[name='subChk']:checked").each(function() { 
        checkedList.push($(this).val()); 
    }); 
    var b=checkedList.toString();
    $.get('rizhi.php',{ids:b},function(data){
        alert("删除成功");
        location.replace(location.href);
    })
}
}

$(function(){
  $("#sta").change(function(){
    var sta=$(this).val();
    var id=$(this).attr('bgid');
    $.get("shensu.php",{id:id,status:sta},function(data){
      alert("处理成功");
    })
  })
})
</script> 
</body>
</html>