<?php
include_once 'is_login.php';

include_once 'top.php';
if(@$_POST['sub']){
  if($_FILES['img']['tmp_name'] != ""){
    $img=$pdo->up($_FILES['img']);
  }else{
    $img='';
  }
    $data=array(
    'name'=>$_POST['name'],
    'pid'=>$_POST['pid'],
    'money'=>$_POST['money'],
    'time'=>time(),
    'img'=>$img,
     'con'=>$_POST['con'],
    'status'=>0,
  );
  $result = $pdo -> insert('w_shop', $data, $debug = false);
  $pdo->jump('添加成功','shop_list.php');
}
?>
<body>
<script type="text/javascript" charset="utf-8" src="../kindeditor/kindeditor.js"></script>
<script type="text/javascript">
KE.show({
      id : 'tx_id',
      imageUploadJson : '../kindeditor/php/upload_json.php',
      fileManagerJson : '../kindeditor/php/file_manager_json.php',
      allowFileManager : true,
      afterCreate : function(id) {
        KE.event.ctrl(document, 13, function() {
          KE.util.setData(id);
          document.forms['example'].submit();
        });
        KE.event.ctrl(KE.g[id].iframeDoc, 13, function() {
          KE.util.setData(id);
          document.forms['example'].submit();
        });
      }
    });
</script>
<article class="page-container">
  <form  method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>商品名称：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="username" name="name">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>商家ID：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="" id="pid" name="pid">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>商品价格</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="" placeholder="请输入商品价格"  name="money">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3">商品图片</label>
      <div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
        <a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
        <input type="file"  name="img" class="input-file">
        </span> </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-3">商品介绍</label>
       <div class="formControls col-xs-8 col-sm-9">
        <textarea id="tx_id" name="con" style="width:800px;height:400px"></textarea>
      </div>
    </div>
  
  
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
        <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" name="sub">
      </div>
    </div>
    
  </form>
  </article>




<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>

<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>