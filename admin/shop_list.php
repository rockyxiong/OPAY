<?php 
include_once 'is_login.php';
if(@$_GET['ids']){
    $ids=explode(',',$_GET['ids']);
    foreach ($ids as $v) {
      $num=$pdo->delete('w_shop','id='.$v);
    }
    echo 1;
    die;
}
include_once 'top.php';

$strSql = 'select * from w_shop where status=0';


$arr = $pdo -> query($strSql, $queryMode = 'All', $debug = false);


if(@$_GET['id']){

  $num=$pdo->delete('w_shop',"id='".$_GET['id']."'");
  $pdo->jump('删除成功','shop_list.php');
}

?>

<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 商家中心 <span class="c-gray en">&gt;</span> 商品管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
 
  <div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> <a href="shop_add.php"  class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加商品</a></span> <span class="r">共有数据：<strong><?php echo count($arr)??0?></strong> 条</span> </div>
  <div class="mt-20">
  <table class="table table-border table-bordered table-hover table-bg table-sort">
    <thead>
      <tr class="text-c">
        <th width="25"><input type="checkbox" name="" value=""></th>
        <th width="20">ID</th>
        <th width="100">商品名称</th>
        <th width="100">商品图片</th>
        <th width="100">商品价格</th>
        <th width="">商品介绍</th>
        <th width="100">添加时间</th>
        <th width="100">操作</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $i=1;
    foreach($arr as $v){
    ?>
      <tr class="text-c">
        <td><input type="checkbox" value="<?php echo $v['id']?>" name="subChk"></td>
        <td><?php echo $i++;?></td>
        
        <td><?php echo $v['name']?></td>
        <td><a href="../<?php echo $v['img']?>" target="_blank"><img src="../<?php echo $v['img']?>" width="20px"></a></td>
        <td><?php echo $v['money']?>元</td>
         <td><?php echo $v['con']?></td>
        <td><?php echo date('Y-m-d',$v['time'])?></td>
        <td class="td-manage">
         <a title="编辑" href="shop_edit.php?id=<?php echo $v['id']?>" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="shop_list.php?id=<?php echo $v['id']?>"  class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>
         </td>
      </tr>
      <?php }?>
    </tbody>
  </table>
  </div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
$(function(){
  $('.table-sort').dataTable({
    "aaSorting": [[ 1, "desc" ]],//默认第几个排序
    "bStateSave": true,//状态保存
    "aoColumnDefs": [
      //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
      {"orderable":false,"aTargets":[0]}// 制定列不参与排序
    ]
  });
  
});


function datadel(){

  if(confirm("确定要删除所选项目？")) { 
    var checkedList = new Array(); 
    $("input[name='subChk']:checked").each(function() { 
        checkedList.push($(this).val()); 
    }); 
    var b=checkedList.toString();
    $.get('shop_list.php',{ids:b},function(data){
        alert("删除成功");
        location.replace(location.href);
    })
}
}
</script> 
</body>
</html>