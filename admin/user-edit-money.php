<?php
include_once 'is_login.php';
$title="会员修改";
include_once 'top.php';
if(@$_POST['sub']){

	$strSqls = 'select * from c_user where uid='.$_POST['hid'];
	$ress = $pdo -> query($strSqls, $queryMode = 'Row', $debug = false);

		$data=array(
		'money'=>$ress['money']+$_POST['changemoney'],
		'jifen'=>$ress['jifen']+$_POST['changejifen']
	);
	$result = $pdo -> update('c_user',$data,'uid ='.$_POST['hid'], $debug = false);
	$result = $pdo -> insert('c_admin_rizhi',array('type'=>'调整资金','uid'=>$_POST['hid'],'money'=>$_POST['changemoney'],'jifen'=>$_POST['changejifen'],'time'=>time(),'info'=>$_POST['info']), $debug = false);
	//zidong('user-list.php','修改成功',1);die;
   $pdo->jump('修改成功','user-list.php');
}else{
	$id=@$_GET['id'];
	$strSql = 'select * from c_user where uid ='.$id;
	$arr = $pdo -> query($strSql, $queryMode = 'Row', $debug = false);
}
?>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>姓名：</label>
			<div class="formControls col-xs-8 col-sm-9"><?php echo $arr['name']?>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>登陆账号：</label>
			<div class="formControls col-xs-8 col-sm-9"><?php echo $arr['tel']?>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>昵称：</label>
			<div class="formControls col-xs-8 col-sm-9"><?php echo $arr['nickname']?>
			</div>
		</div>
	
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>当前余额:</label>
            <div class="formControls col-xs-8 col-sm-9"><?php echo $arr['money']?>
            </div>
        </div>
	
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>当前积分:</label>
			<div class="formControls col-xs-8 col-sm-9"><?php echo $arr['jifen']?>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*增减金额:</span></label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="0" placeholder=""  name="changemoney">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*增减积分:</span></label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="0" placeholder=""  name="changejifen">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*调整理由:</span></label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="" placeholder="请填写变更的理由"  name="info">
			</div>
		</div>
	
		<input type="hidden" name="hid" value="<?php echo $arr['uid']?>">
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
		说明:调整金额填写数字，默认为用户当前余额加上填写当金额；<br>
		如果要减少，请填写负数，即如果要减少用户1000金额，请在 增减金额 中填写 -1000<br>
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交变更&nbsp;&nbsp;" name="sub">
			</div>
		</div>
	</form>
</article>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>

<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>