<?php
$config = array (	
		//应用ID,您的APPID。
		'app_id' => "2018091261348284",

		//商户私钥，您的原始格式RSA私钥
		'merchant_private_key' => "MIIEogIBAAKCAQEAzymhdgNdgSvuUShYFnVwGodrYgvOvEEBP5JCpLwtqphHbRNn6QKXIjoG8uHYxVcxapq7hWx1q+5p3s13qFE/QJISgNjgBE4khNMa0FHcBM0zOpkHZlGB/LA5rYs8iWNwapKN0zcqJo6umiUj+e96KmjaIa3sQIEwR/0wpDGXN0OXHv4ReAoZMbBVIKnLeApLjONBf79VE/9I+6ptaDlem3LGM5CdQhBjYHgjAI59lI1SjQe52iyAlL3QWEvMyX6wvIY6bRW5YXRj1HyQZnFx25THOZaxlcbrEm4aoYy06+1vwecIsv0Fez3huK3k0K7gKK07nnFLEXdSvB08om5YfQIDAQABAoIBAHOTY0wm1nzhWIUYwARPoqp6ULOTqNQoZBzmaZ3kiom5iWfWQsuhsVXcD9Jjo/99BMnX57SpbQDt80zlTtOt+F76363Y0So1unQMHtKISPq4zNLtW05/PMkQPC2RF8YCn04l01A6X57BTbIb9HVVYM/WTZQqpzrZmGkhXYjNTfL7FmPnKVWBIjMEGwBaJU/V2qrPZI7M/j06HmHzQATmFk4GV9KTCLRTAoNTr1aqVOUFLyc8xmZHocB1nExz/ny3RfD2Kl/X/zlvl17xENjMxZNC5x0hBJJloKhZ7MSwTYTKtQvXs4DP1ux9TteiJ0qKcNCBZv1PEBFbJ9pt1vXP5bUCgYEA75or7vxO3aQVb9tMVWFTZkCkX2UMx+QAsz/2WEyoVMw72D0XJykJtgBrXkgNFP7ANgAVhH1XLAriLFQIuL3JAJ+zFKz2/WPUuN49j3bQVYRknMylEyt75DsJ003sPnZdOHv51wHRbQLlDGQH6dBY8JQjFdfAbmP1cejP0Mlx4AMCgYEA3VceEMdRSUoNJdYtrXi8YaYamvPZQZNUs1JRVsLB4VF9MuTt7mPVVL1J+ph/FkrwfU2MCVQ/xonDsJroHR8yZzBr65oc2bWkMv/glNUDCiZwh2gh/rvGxdpAVN0N0cmhUKLajasud9VR3xPK3ZGUO9XOokYbGhQ4LYKOEkravX8CgYA8k3TSCfYCcS5+RipJV77XFja4a3rMuqc2qlAkyNSyA6SJqgBFhUmQOxDqGDs+eJ2tyV2nTm0QlHvUCzbdRhYgUz/aVVRoganRR1a3A6nuRPeO8Oo3K4I6FHcKdiPpMtGkqytiIXKKJfY0qMGbanJh+IHaTt62ygzOzLZlqdhhcwKBgEVq7yj/+gRsTxdQxt6jASH3AF0coUSpumTLKmt1hWzvK77sgZrEgMGk5DDN5v3M3wPNcF/0plPUHSWkT6vifuJSE2a+3NGC4E6GZZ8H934VdDFjfVXw+nRO8OIsugrTm944VScdSuOWaaj3AibfY4golDfcmN9i/AN58KjYBSZtAoGAW1AOtzrBA8Sp/3yfpD/ui2VgVSft8ZgRleQZZa0TwEGguPcGvuDpGQ+HJA0V7UJQxsEfKlqz9sK0wrDJaUwoLn2Fy+Rp8citjXHbUdx4fH3usa/kwBe61yXxFzn8UEgRnp6OsVLoifyBnnNsTfzSFnNRL1VO1Am4ck3Jllg6qJU=",
		
		//异步通知地址
		'notify_url' => "http://2e702070.ngrok.io/OPAY/api/alipayWap/notify_url.php",
		
		//同步跳转
		'return_url' => "http://2e702070.ngrok.io/OPAY/api/alipayWap/return_url.php",

		//编码格式
		'charset' => "UTF-8",

		//签名方式
		'sign_type'=>"RSA2",

		//支付宝网关
		'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

		//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
		'alipay_public_key' => "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzymhdgNdgSvuUShYFnVwGodrYgvOvEEBP5JCpLwtqphHbRNn6QKXIjoG8uHYxVcxapq7hWx1q+5p3s13qFE/QJISgNjgBE4khNMa0FHcBM0zOpkHZlGB/LA5rYs8iWNwapKN0zcqJo6umiUj+e96KmjaIa3sQIEwR/0wpDGXN0OXHv4ReAoZMbBVIKnLeApLjONBf79VE/9I+6ptaDlem3LGM5CdQhBjYHgjAI59lI1SjQe52iyAlL3QWEvMyX6wvIY6bRW5YXRj1HyQZnFx25THOZaxlcbrEm4aoYy06+1vwecIsv0Fez3huK3k0K7gKK07nnFLEXdSvB08om5YfQIDAQAB",
		
	
);