<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/10
 * Time: 11:07
 */

/**
 * json输出格式
 */
function arrayToJson($datas)
{
    header('Content-type: application/json');
    if(!isset($datas['update'])){
        $datas['update'] = date("Y-m-d H:i:s");
    }
    //回调函数
    //array_walk_recursive($datas, 'dealResult');
    echo json_encode($datas);
    exit;
}
/**
 * json输出错误信息
 */
function jsonError($params)
{
    if(is_array($params)){
        $result = array('status' => '0', 'data' => $params);
    }else{
        $result = array('status' => '0', 'data' => array('msg' => $params));
    }
    arrayToJson($result);
}
/**
 * json输出正确信息
 */
function jsonSuccess($params){
    if(is_array($params)){
        $result = array('status' => '1', 'data' => $params);
    }else{
        $result = array('status' => '1', 'data' => array('msg' => $params));
    }
    arrayToJson($result);
}

/**
 * 打印输出
 * @param $var
 * @param bool $exit
 */
function dump($var, $exit = true)
{
    echo '<pre>';
    print_r ( $var );
    echo '</pre>';
    if ($exit) {
        die ();
    }
}

/**
 * app 调试日志
 * @param unknown $data
 */
function htmlLog($data = array()){
    $postData = $_REQUEST;
    $returnData = $data;
    $content = date('Y-m-d H:i:s'). '<br>';
    $content .= 'post data ----------------------------<br>';
    $content .= var_export($postData, true).'<br>';
    $content .= 'return data ----------------------------<br>';
    $content .= var_export($returnData, true).'<br>';
    $logpath =  '../api/logs/';
    if(!is_dir($logpath)){
        $res = @mkdir($logpath);
    }
    $filename = $logpath.'log_'. date('Ymd'). '.html';
    file_put_contents($filename, $content, FILE_APPEND);
}

function account(){
    $numbers = range (1,8);
    shuffle ($numbers);
    return implode("",$numbers);
}
