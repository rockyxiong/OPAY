<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/10
 * Time: 15:31
 */
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/5
 * Time: 17:06
 */

require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'juhe/flow.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'common/function.php';

$prepaid = new flow();

$act = $_GET['act'];
$mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : "18062551673";
$pid = !empty($_POST['pid']) ? $POST['pid'] : 28;//流量套餐ID

if ( $act == 'getlist' ){
    //获取支持的流量套餐列表
    $res = $prepaid->getList();
    if( 0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

}elseif ( $act == 'telquery' ){
    //根据手机号码获取支持的流量套餐
    $res = $prepaid->telquery($mobile);
    if( 0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

} elseif ( $act == 'flowcz' ){

    //生成流量订单号
    $orderid = 'LL'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    //提交话费充值
    $res = $prepaid->flowcz($mobile,$pid,$orderid);
    if(0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

} elseif ( $act == 'ordersta' ){

    //查询流量订单的充值状态
    $orderid = 'LL'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    $res = $prepaid->ordersta($orderid);
    if(0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

} else {
    jsonError("你的访问的页面地址不正确！");
}