<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/5
 * Time: 17:34
 */
class flow{

    private $appkey = 'e28ff8735ea47da41fd3da1b5aac6d30';

    private $openid = 'JH4f386552e632cdfcfb046bb2a546a1fb';

    private $flowList = 'http://v.juhe.cn/flow/list';

    private $telcheck = 'http://v.juhe.cn/flow/telcheck';

    private $submitUrl = 'http://v.juhe.cn/flow/recharge';

    private $staUrl = 'http://v.juhe.cn/flow/ordersta';

    public function __construct(){

    }

    /**
     * 获取支持的流量套餐列表
     * @return string
     */
    public function getList(){
        $params = 'key='.$this->appkey;
        $content = $this->juhecurl($this->flowList,$params);
        return $this->_returnArray($content);;
    }

    /**
     * 根据手机号码获取支持的流量套餐
     * @param  string $mobile   [手机号码]
     * @return  array
     */
    public function telquery($mobile){
        $params = 'key='.$this->appkey.'&phone='.$mobile;
        $content = $this->juhecurl($this->telcheck,$params);
        return $this->_returnArray($content);
    }

    /**
     * 提交话费充值
     * @param  [string] $mobile   [手机号码]
     * @param  [int] $pid  套餐的id
     * @param  [string] $orderid  [自定义单号]
     * @return  [array]
     */
    public function flowcz($mobile,$pid,$orderid){
        $sign = md5($this->openid.$this->appkey.$mobile.$pid.$orderid);//校验值计算
        $params = array(
            'key' => $this->appkey,
            'phone'   => $mobile,
            'pid'   => $pid,
            'orderid'   => $orderid,
            'sign' => $sign
        );
        $content = $this->juhecurl($this->submitUrl,$params,1);
        return $this->_returnArray($content);
    }

    /**
     * 查询订单的充值状态
     * @param  [string] $orderid [自定义单号]
     * @return  [array]
     */
    public function ordersta($orderid){
        $params = 'key='.$this->appkey.'&orderid='.$orderid;
        $content = $this->juhecurl($this->staUrl,$params);
        return $this->_returnArray($content);
    }

    /**
     * 将JSON内容转为数据，并返回
     * @param string $content [内容]
     * @return array
     */
    public function _returnArray($content){
        return json_decode($content,true);
    }

    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    public function juhecurl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'JuheData' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return $response;
    }
}