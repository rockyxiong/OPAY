<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/12
 * Time: 14:27
 */
class sms{

    protected $sendUrl = 'http://v.juhe.cn/sms/send';
    protected $appkey = '9e179a3cba7261de7154bf3a4d828ae3';

    protected $tpl_id = '100776'; //短信模板id
    protected $pdo = null;
    public function __construct()
    {

    }

    /**
     * 短信发送
     */
    public function sendSms($mobile,$code)
    {
        $smsConf = array(
            'key'   => $this->appkey, //您申请的APPKEY
            'mobile'    => $mobile, //接受短信的用户手机号码
            'tpl_id'    => $this->tpl_id, //您申请的短信模板ID，根据实际情况修改
            'tpl_value' => urlencode('#code#='.$code) //您设置的模板变量，根据实际情况修改urlencode("#code#=1234&#company#=聚合数据")
        );
        header('content-type:text/html;charset=utf-8');
        $content = $this->juhecurl($this->sendUrl,$smsConf,1); //请求发送短信
        return $content;
    }

    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    function juhecurl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        if ($response === FALSE) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return $response;
    }
}