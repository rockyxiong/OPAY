<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/11
 * Time: 15:13
 */

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'common/function.php';
require_once dirname( __FILE__ ).DIRECTORY_SEPARATOR.'payment/alipayWap.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'juhe/telephone.php';
Session_start();
//账户余额
$prepaid = new telephone();
$yueQuery = $prepaid->yueQuery();
$remainMoney = $yueQuery['result']['money'];

$alipay = new alipayWap();
$subjectArr = [
    '1'=>'OPAY话费充值',
    '2'=>'OPAY流量充值'
];
//订单类型
$subject = !empty($_POST['subject']) ? $_POST['subject'] : 1;
if (1 == $subject){
    $prefix = 'HF';
} elseif (2 == $subject){
    $prefix = 'LL';
}
//生成订单号
$orderid = generate_trade_no($prefix);
$total_amount = !empty($_POST['total_amount']) ? $_POST['total_amount'] : '0';

if($total_amount > $remainMoney){
    jsonError("账户余额不足，请联系客服！");
}
if($total_amount <= 0){
    jsonError("订单金额要大于0");
}
$mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '18062551673';
$pid = !empty($_POST['pid']) ? $_POST['pid'] : ''; //流量套餐id
$body = '充值中心';
$content = [
    'body'=>$body,
    'subject'=>$subjectArr[$subject],
    'out_trade_no'=>$orderid,//此订单号为商户唯一订单号
    'total_amount'=> $total_amount,//保留两位小数
    'product_code'=>'QUICK_MSECURITY_PAY'
];
$response = $alipay->dopay($content);

//生成订单
$orderArr = [
    'order_id'=>$orderid,
    'user_id'=>!empty($_SESSION['uid']) ? $_SESSION['uid'] : 0,
    'shop_type'=>$subject, //1 话费充值 2 流量充值
    'payment_type'=>1,//支付方式（1 支付宝 2 微信）
    'mobile'=>$mobile,
    'pid'=>$pid,
    'total_amount'=>$total_amount,
    'status'=>1, //待支付
    'create_time'=>date('Y-m-d H:i:s',time()),
    'update_time'=>date('Y-m-d H:i:s',time())
];
$alipay->createOrder($orderArr);

/**
 * 生成订单号
 * @param $prefix
 * @return string
 */
function generate_trade_no($prefix){
    if(empty($prefix)){
        $prefix = '';
    }
    $orderid = $prefix.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    return  $orderid;
}




