<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/11
 * Time: 14:30
 */
class alipayApp{

    //支付宝网关
    protected $gatewayUrl = "https://openapi.alipay.com/gateway.do";
    //应用ID
    protected $appId = "2016091700531038";
    //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    protected $rsaPrivateKey = 'MIIEpAIBAAKCAQEAwS5LiFOY796EDuEedFqrPGEl0E18jWY+k9QaRNEarSB8KQkSpITQwnpwKVCargiDTsA+G3RRgdJI6yIPNI2b+OiZQpeVrMOAvMmtkPfjjy+4jkmreumfMLyUUx9njGdlVewQ1pLxFZUp4gTD4nAAJEXfLNBxF706GpEhBuf2vzYKT82PlVtRTpX/Y84Uzb4N2M2IedTQeS/IvsLZxBD53unrzSWwsuK3F72LtnOB8I4WOffLl+IRZhyTGGWafRdSAwtWlTEazxA2yHrLKxwu5vBHesSUsR5HkkMEu8EeVGbMODjpmzYh5LLFKhqrM7dA+qhytP2kpHFltI0UP5dT9wIDAQABAoIBAA3Eve9ufeUCyAjDdMc4OyInjmllTG5LjkQ9Bs5G1Q7n1NGb6u885CAoV9wp+YxT4P/45bRROwn0sevIVqsZOLep5FqBsVJJ8OADw9IkSNzIuu+xQn6VLhpsJmoyP9AbAnb4R07z1CkEo6KPUmLONsRfEoQAKz9U5lJEa8UxPT3tfVJCIGbJ3llmE7Xo+54sJXGJH2Uygigy+mTWgOnUGNARHHUCLkOG6P2Ir4BPA8fLxmp4QOYLXlWtA5+6XQSeNNrR9FiyAmROdLoiUNwxj1x/t7TGmlAa3Y302yMIMR9UQbnNPMWSxOkPTPk3d4BDqQnRniVnh5Qh+HlI6wzUTOECgYEA7jHf6cJgztRuu5JuyuJZu2W+sTlhq9vLtZgO1FNreaXu5ds29i/B2QyXgqIQTSQ+tT2WK782oLAlqXH3F8w/BNxeGNWfCHiE/K3LpACsRZ0rR8NvV9ICmiSsmyCD1p9XK3lnaiho4zEY1ZIv/ih2QUgDP6CSDL4NvqCNhbQCyjECgYEAz58G/KRS2Yy1lzvSoIE+vvu2t1ZEl9VlP3/g69PKWa9PuZfn3ORKU8w/rtBmRUAV0hexZvDvKPeMaGBVzosuRDdZKMbXH2tg6ioVF8rG987/y3VwXKzXTEQvs1Oy/tdZVt0Ew9tlsH0cwbE2ixQtsMAlgL0vvvDFuPxa9I2yzqcCgYEAnUAr9OUwiBtSp1566lIYr1DyXYnmY1EN4Wrrj10rPPpBB1p7pHfO3/4iIEnhvSvKgliQRX1NETR5CK6GVB2xFQJu1LJi9XKrSS1rnXAcSzEBm7T4UuAAe8budNM4dn5nS/owPbND24lUPdru+Dm6KErkaiMViHgIFutjwvseRGECgYAq1AQt83u/sr32z3lYcLDP1r7PPPVzWKrFGYz9OLgiHf7VKxfCQjlu5dAxMH4YROr7ZrXKpWLAUaheE5k/vQgzPDOhLLtNmOnqA/pL6SF4FvQ/NR+TjnfvHJW2aUqgNS4RtA/6kvR8HqLi9bppoCT4s/pIfz4tJh+zVCMzFVPQOwKBgQCZwvoef+B2XENc4qoPfznbP3/G4YCxCm4rF/odh1A/c7FX7KRbDl+2X6treLaqkAUxcdaAtPPjK9m4GGJqXycfJWrDsJXa7lBzgKBfsrdUUwBWN5KHLXaIhzp52lMe/wZqMy9mymZ6uUN7Jiwz/VabfUGBHbE7MnmbodI86Cyhzg==';

    protected $alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwS5LiFOY796EDuEedFqrPGEl0E18jWY+k9QaRNEarSB8KQkSpITQwnpwKVCargiDTsA+G3RRgdJI6yIPNI2b+OiZQpeVrMOAvMmtkPfjjy+4jkmreumfMLyUUx9njGdlVewQ1pLxFZUp4gTD4nAAJEXfLNBxF706GpEhBuf2vzYKT82PlVtRTpX/Y84Uzb4N2M2IedTQeS/IvsLZxBD53unrzSWwsuK3F72LtnOB8I4WOffLl+IRZhyTGGWafRdSAwtWlTEazxA2yHrLKxwu5vBHesSUsR5HkkMEu8EeVGbMODjpmzYh5LLFKhqrM7dA+qhytP2kpHFltI0UP5dT9wIDAQAB';
    protected $signType = 'RSA2';
    protected $charset = 'UTF-8';
    protected $format = 'json';
    protected $notifyUrl = 'http://39c65679.ngrok.io/OPAY/api/notify.php';

    public function __construct()
    {
        require_once('./alipayApp/aop/AopClient.php');
        require_once('./alipayApp/aop/request/AlipayTradeAppPayRequest.php');
    }

    public function pay($content = array())
    {
        $aop = new \AopClient();
        //**沙箱测试支付宝开始
        $aop->gatewayUrl = $this->gatewayUrl;
        //实际上线app id需真实的
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA2";
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;

        $bizcontent = json_encode($content);
        //**沙箱测试支付宝结束
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        $request = new \AlipayTradeAppPayRequest();
        //支付宝回调
        $request->setNotifyUrl("http://39c65679.ngrok.io/api/notify.php");
        $request->setBizContent($bizcontent);
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        echo $response;
    }

    public function notifyurl()
    {
        $aop = new \AopClient;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");
        if($flag){
            //验证成功
            //这里可以做一下你自己的订单逻辑处理
            //校验通知数据的正确性

            $out_trade_no = $_POST['out_trade_no']; //商户订单号

            $trade_no = $_POST['trade_no'];//支付宝交易号

            $trade_status = $_POST['trade_status'];//交易状态trade_status

            $total_amount = $_POST['total_amount'];//订单的实际金额

            $app_id = $_POST['app_id'];

            if($app_id!=$this->app_id) exit('app_id错误');//验证app_id是否为该商户本身

            echo 'success';//这个必须返回给支付宝，响应个支付宝，
        } else {
            //验证失败
            echo exit('签名验证失败');
        }
        //$flag返回是的布尔值，true或者false,可以根据这个判断是否支付成功
    }
}