<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/12
 * Time: 13:48
 */

class alipayWap
{
    protected $config = [];
    protected $pdo = null;

    public function __construct()
    {
        include_once dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR."pdoclass.php";
        $this->pdo = $pdo;
    }

    /**
     * 发起订单
     * @param float $totalFee 收款总费用 单位元
     * @param string $outTradeNo 唯一的订单号
     * @param string $orderName 订单名称
     * @param string $notifyUrl 支付结果通知url 不要有问号
     * @param string $timestamp 订单发起时间
     * @return array
     */
    public function doPay($content)
    {
        header('Content-type:text/html; Charset=utf-8');
        require_once '../api/alipayWap/wappay/service/AlipayTradeService.php';
        require_once '../api/alipayWap/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';
        require_once '../api/alipayWap/config.php';
        $this->config = $config;
        //if (!empty($_POST['WIDout_trade_no']) && trim($_POST['WIDout_trade_no']) != "") {
            //超时时间一分钟
            $timeout_express = "1m";
            $payRequestBuilder = new AlipayTradeWapPayContentBuilder();
            $payRequestBuilder->setBody($content['body']);
            $payRequestBuilder->setSubject($content['subject']);
            $payRequestBuilder->setOutTradeNo($content['out_trade_no']);
            $payRequestBuilder->setTotalAmount($content['total_amount']);
            $payRequestBuilder->setTimeExpress($timeout_express);
            $payResponse = new AlipayTradeService($this->config);
            $result = $payResponse->wapPay($payRequestBuilder, $this->config['return_url'], $this->config['notify_url']);

            return;
       // }
    }

    /**
     * 创建订单
     */
    public function createOrder ($orderArr){
        $result = $this->pdo->insert('w_order', $orderArr, $debug = false);
        return $result;
    }

    /**
     * 改变订单状态
     * @param $order_id 订单编号
     * @param $juhe_amount 聚合订单金额
     */
    public function updateOrder($order_id,$juhe_amount){
        $result = $this->pdo->update('w_order',array('status'=>2,'juhe_amount'=>$juhe_amount,'update_time'=>date('Y-m-d H:i:s',time())),"order_id ='$order_id'", false);
        return $result;
    }

    /**
     * 查询订单信息
     */
    public function orderInfo($order_id){
        $strSql = "select * from w_order where order_id ='{$order_id}'";
        $res = $this->pdo->query($strSql, $queryMode = 'Row', $debug = false);
        return $res;
    }
}
?>