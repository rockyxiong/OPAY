<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/5
 * Time: 17:06
 */
require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'juhe/telephone.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'common/function.php';
require_once dirname ( __FILE__ ).DIRECTORY_SEPARATOR.'payment/alipayApp.php';

$act = @$_GET['act'];
$prepaid = new telephone();
$mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : "18062551673";
$pervalue = !empty($_POST['money']) ? $_POST['money'] : 1;

if ( $act == 'telquery' ){
    //根据手机号码及面额查询是否支持充值
    if(!$prepaid->telcheck($mobile,$pervalue)){
        jsonError("手机号码或者面额不支持充值！");
    }
    //根据手机号码和面额获取商品信息
    $res = $prepaid->telquery($mobile,$pervalue);
    if(0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['result']);

} elseif ( $act == 'telcz' ){
    //生成订单号
    $orderid = 'HF'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    $content = [
        'body'=>'充值',
        'subject'=>'话费',
        'out_trade_no'=>$orderid,//此订单号为商户唯一订单号
        'total_amount'=> '0.01',//保留两位小数
        'product_code'=>'QUICK_MSECURITY_PAY'
    ];
    $alipay = new alipayApp();
    $response = $alipay->pay($content);

    //提交话费充值
    $res = $prepaid->telcz($mobile,$pervalue,$orderid);
    if(0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

} elseif ( $act == 'sta' ){
    //查询订单的充值状态
    $orderid = 'HF'.date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    $res = $prepaid->ordersta($mobile,$pervalue,$orderid);
    if(0 == $res['error_code']){
        jsonSuccess($res['result']);
    }
    jsonError($res['reason']);

} else {
    jsonError("你的访问的页面地址不正确！");
}





