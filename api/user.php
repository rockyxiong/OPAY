<?php
/**
 * Created by PhpStorm.
 * User: xiong
 * Date: 2018/9/14
 * Time: 14:08
 */
include_once "../pdoclass.php";
include_once "../api/common/function.php";
include_once "../api/juhe/sms.php";
global $pdo;
Session_start();
$act = !empty($_GET['act']) ? $_GET['act'] : '';

//查询账号是否存在
if($act == 'checkMobile'){
    $mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '';
    if(!checkMobile($mobile)){
        jsonError("手机格式不对");
    }
    $user = checkCode($mobile);
    if(empty($user['uid'])){
        jsonError("该账号未注册");
    }
    jsonSuccess("该账号已注册");

} elseif ($act == 'sendCode'){
    $sms = new sms();
    $code = rand(100000,999999);
    $mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '';
    if(!checkMobile($mobile)){
        jsonError("手机格式不对");
    }
    $user = checkCode($mobile);
    if(empty($user['uid'])){
        jsonError("该账号不存在");
    }
    $count = checkSmsSendTimes($mobile,1);
    if($count >= 5){
        jsonError("24小时内只能发送五次，如有问题请联系客服！");
    }
    //发送验证码
    $content = $sms->sendSms($mobile,$code);
    if($content['error_code'] == 0){
        insertSmsLog($mobile,$code,1);
        jsonSuccess("发送成功，请验收！");
    }
    jsonError("发送失败，请联系客服！");

} elseif ($act == 'checkCode'){
    $code = !empty($_POST['code']) ? $_POST['code'] : '';
    $mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '';
    $getCode = getCode($mobile ,1);
    if($getCode['code'] != $code){
        jsonError("验证码不正确");
    }
    $waitTime = time() - strtotime($getCode['create_time']);
    if($waitTime > 3*60){
        jsonError("验证码超时");
    }
    jsonSuccess("验证通过！");

} elseif ($act == 'doLogin'){
    $mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '';
    $password = !empty($_POST['password']) ? $_POST['password'] : '';
    if(!checkMobile($mobile)){
        jsonError("手机格式不对");
    }
    if(!$mobile || !$password){
        jsonError("账号和密码不能为空");
    }
    if(checkPwd($mobile,sha1($password))){
        $_SESSION['uid'] = $res['uid'];
        $_SESSION['name'] = $res['name'];
        jsonSuccess("登录成功");
    }
    jsonError("账号或密码错误");

} elseif ($act == 'doReg'){
    $mobile = !empty($_POST['mobile']) ? $_POST['mobile'] : '';
    $password = !empty($_POST['password']) ? $_POST['password'] : '';
    if(!checkMobile($mobile)){
        jsonError("手机格式不对");
    }
    if(!$mobile || !$password){
        jsonError("账号和密码不能为空");
    }
    if(strlen($password) < 6 || strlen($password)>16){
        jsonError("密码长度要8到16位");
    }
    $data['tel'] = $mobile;
    $data['pwd'] = sha1($password);
    $data['nickname'] = '请及时更换昵称';
    $data['sex'] = 1;
    $data['regtime'] = time();
    $data['account'] = account();
    $user_id = insertUser($data);
    if($user_id){
        jsonSuccess("注册成功");
    }
    jsonError("注册失败");

}else{
    jsonError("你访问的url不存在");
}

/**
 * @param $mobile
 * @param $password
 * @return mixed
 */
function checkPwd($mobile,$password){
    $strSql = "select * from `c_user` where `tel` = '".$mobile."' and `pwd` = '".$password."'";
    $res = $GLOBALS['pdo'] -> query($strSql, $queryMode = 'Row', $debug = false);
    return $res;
}

/**
 * @param $mobile
 * @return bool
 */
function checkCode($mobile){
    if(!$mobile){
        return false;
    }
    $strSql = "select uid, tel from `c_user` where `tel` = '{$mobile}'";
    $user = $GLOBALS['pdo'] -> query($strSql, $queryMode = 'Row', $debug = false);
    return $user;
}

/**
 * 24小时内发送的次数
 */
function checkSmsSendTimes($mobile ,$type){
    if(!$mobile){
        return false;
    }
    $start_time = date("Y-m-d H:i:s",time() - 24*3600);
    $end_time = date("Y-m-d H:i:s",time());
    $strSql = "select id from `w_sms_log` where `mobile` = '{$mobile}' AND type = '$type' AND create_time >'$start_time' AND create_time < '$end_time'";
    $count = $GLOBALS['pdo'] -> getCount('w_sms_log', 'id', $where = "mobile = '{$mobile}' AND create_time >'$start_time' AND create_time < '$end_time'", $debug = false);
    return $count ? $count : 0;
}

/**
 * 發送短信記入日誌.
 * @param array
 * @return 無
 */
function insertSmsLog($mobile,$code,$type = 1){
    if(!$mobile || !$code){
        return false;
    }

    $arrayDataValue = [
        'mobile'=>$mobile,
        'code'=>$code,
        'type'=>$type,
        'create_time'=>date('Y-m-d H:i:s',time())
    ];
    $res = $GLOBALS['pdo']->insert('w_sms_log', $arrayDataValue, $debug = false);
    return $res;
}

/**
 * 注册数据
 * @param array $data
 * @return mixed
 */
function insertUser($data=[]){
    $res = $GLOBALS['pdo']->insert('c_user',$data , false);
    return $res;
}

/**
 * 获取验证码
 * @param $mobile
 * @param int $type
 * @return bool
 */
function getCode($mobile ,$type = 1){
    if(!$mobile){
        return false;
    }
    $sql = "SELECT code,create_time FROM w_sms_log WHERE mobile = '{$mobile}' AND type = '{$type}' ORDER BY id DESC LIMIT 1";
    $user = $GLOBALS['pdo'] -> query($sql, $queryMode = 'Row', $debug = false);
    return $user ? $user: '';
}

/**
 * 检查手机号
 * @param $mobile
 * @return bool
 */
function checkMobile($mobile){
    $preg = '/^1([358]\d|4[579]|66|7[0135678]|9[89])\d{8}$/';
    if ( !preg_match($preg, $mobile)) {
        return false;
    }
    return true;
}
