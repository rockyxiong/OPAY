<?php
function login($tel){
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="window-target" content="_top">
		<meta content="telephone=no" name="format-detection">
		<meta name="Keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" href="styles/ume.css">
		<link rel="stylesheet" href="styles/animate.css">
		<link rel="stylesheet" href="styles/styles.css">

		<script src="js/jquery.min.js"></script>
		<script src="js/ume.js"></script>
		<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

	</head>
<section class="tip animated fadeIn" style="display:block">
			<div class="tip-center center">
				<div class="tip-main animated zoomIn">
					<div class="block" style="text-align: center; font-size: .3rem;">
						<h2>提示</h2><br />
						<p class="p1">输入密码有误，请重新输入！</p>
						<p class="p2">
							<a href="login3.php?tel=<?php echo $tel?>" onclick="$('.tip').toggle()">取消操作</a>
							<a href="#" onclick="$('.tip').toggle()">忘记密码</a>
						</p>
					</div>
				</div>
			</div>
		</section>
		<?php }?>