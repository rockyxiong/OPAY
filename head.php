<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="window-target" content="_top">
		<meta content="telephone=no" name="format-detection">
		<title><?php echo $title?></title>
		<meta name="Keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" href="styles/ume.css?v=2">
		<link rel="stylesheet" href="styles/animate.css?v=2">
		<link rel="stylesheet" href="styles/styles.css?v=2">
		<link rel="stylesheet" href="styles/login.css?v=2">

		<script src="js/jquery.min.js"></script>
		<script src="js/ume.js"></script>
		<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

	</head>

	