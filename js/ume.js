//rem adaptive
(function (doc, win) {
    var docEl = doc.documentElement,
    resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
    var recalc = function () {
        var width = docEl.clientWidth;
        if (width > 720) {
            width = 720 ;
        }
        if (width < 320) {
            width = 320 ;
        }
        docEl.style.fontSize = 100 * (width / 720) + 'px';
    };
    recalc();
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, recalc, false);
})(document, window);


//gotop
$(function(){
	$(window).scroll(function(){
		var scrH=document.documentElement.scrollTop+document.body.scrollTop;
		if(scrH>200){
			$('.gotop').fadeIn(400);
		}else{
			$('.gotop').stop().fadeOut(400);
		}
	});
	$('.gotop').click(function(){
		$('html,body').animate({scrollTop:'0px'},600);
	});
});

