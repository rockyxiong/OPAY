$(function () {//init事件
    $("#admin,#pwd,#enter_pwd").keyup(keyup);
    //忘记密码
    $("#contant4>h6").click(forget_pwd);
});
//登陆密码正则
var regLogpwd=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/;
function keyup() {
    var dom=$(this);
    if (dom.val()) {
        if(dom.prop('id') == 'admin'){
            var admin_val=$.trim(dom.val());
            $('#login1_label').addClass('show2 animated fadeIn');
            if(!(/^1[34578]\d{9}$/.test(admin_val))){
                dom.next().prop('disabled', true).addClass('disabled_true');
                if(admin_val.length==11){
                    $('#login1_label').text('手机号码错误').addClass('red');
                }else{
                    $('#login1_label').text('手机号码').removeClass('red');
                }
                  
            }else{
                //TODO 注册或登陆标识
                dom.next().prop('disabled',false).addClass('disabled_false').removeClass('disabled_true');
$
            }
        }else if(dom.prop('id') == 'pwd'){
            var pwd_val=$.trim(dom.val());
            $('#login3_label').addClass('show2 animated fadeIn');
            if(!(regLogpwd).test(pwd_val)){
                dom.next().prop('disabled', true).addClass('disabled_true');
                if (pwd_val.length>=8) {
                    $('#login3_label').text('登陆密码错误').addClass('red');
                } else {
                    $('#login3_label').text('登陆密码').removeClass('red');
                }
            }else{
                dom.next().prop('disabled',false).addClass('disabled_false').removeClass('disabled_true');
            }
        }else if(dom.prop('id') == 'enter_pwd'){
            var enter_pwd_val=$.trim(dom.val());
            $('#login4_label').addClass('show2 animated fadeIn');
            if(!(regLogpwd).test(enter_pwd_val)){
                dom.next().prop('disabled', true).addClass('disabled_true');
                if (enter_pwd_val.length>=8) {
                    $('#login4_label').text('登陆密码错误').addClass('red');
                } else {
                    $('#login4_label').text('登陆密码').removeClass('red');
                }
            }else{
                dom.next().prop('disabled',false).addClass('disabled_false').removeClass('disabled_true');
            }
        }
       
    } else {
        if(dom.prop('id') == 'admin'){
            $('#login1_label').removeClass('show2 animated fadeIn');
        }else if(dom.prop('id') == 'pwd'){
            $('#login4_label').removeClass('show2 animated fadeIn');
        }else if(dom.prop('id') == 'enter_pwd'){
            $('#login4_label').removeClass('show2 animated fadeIn');
        }
        dom.next().prop('disabled', true).addClass('disabled_true');
    }
}
//下一步click事件
function fun(key1,key2) {
    $('.login').addClass('hide');
    $('#contant'+key2).removeClass('hide');
    //回显手机号
    $('.tel').text($('#admin').val());
}
function forget_pwd(){
$('#contant4').addClass('hide');
$('#contant2').removeClass('hide');
// TODO 清空原验证码
}
