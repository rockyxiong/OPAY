<?php
Session_start();

$title="登陆";
include_once"head.php";
?>
<body>
		<section class="main animated fadeIn">
			<section class="title block animated fadeInDown">
				<a href="login.php" class="fl ta-black"></a>
				<h1></h1>
				<a class="fr"></a>
			</section>
			<section class="login contant" id="contant1">
				<h2>欢迎来到OPAYCOIN！</h2>
				<div class="log contant"><label id="login1_label" style="visibility: hidden;">手机号码</label>
					<input type="text" placeholder="请输入手机号码" id="admin" maxlength="11" />
					<!-- <span style="display: inline-block;width:10%;background-image: url(../images/fanhui_03.png);">待开发</span> -->
					<a class="btn" disabled="true" id="a" style="opacity: 0.36;pointer-events:none;" onclick="return fun(this,2)">下一步</a>
					<!-- <span style="display：inline-block;width: 100%;height: 0.1rem;background-image: url(images/yingzi.png);"></span> -->
				</div>
			</section>
			<section class="login contant hide" id="contant2">
				<h2>请输入验证码</h2>
				<h6 style="padding-left: 1.8em;color: #969696;">验证码已发送至<span class="tel">182 0735 3312</span></h6>
				<div class="log contant"><label id="">&nbsp;</label>
					<input type="number" placeholder="请输入验证码" oninput="if(value.length>6)value=value.slice(0,6)" id="" />
					<a class="btn" id="b" onclick="return fun(this,3)">下一步</a>
				</div>
			</section>
			<section class="login contant hide" id="contant3">
				<h2>请设置登陆密码</h2>
				<div class="log contant"><label id="login3_label" style="visibility: hidden;">设置密码</label>
					<input type="password" placeholder="6-18位登录密码" id="pwd" maxlength="16" />
					<a class="btn" disabled="true" id="c" style="opacity: 0.36;pointer-events:none;" onclick="return fun(this,4)">注册</a>
				</div>
				<h6 style="padding-left: 1.8em;margin-top: -0.8rem;color: #969696;">注册代表您同意<span style="color: #FA7C25;">《OPAYCOIN相关协议》</span></h6>
			</section>
			<section class="login contant hide" id="contant4">
				<h2>请输入登陆密码</h2>
				<h6 style="padding-left: 1.8em;color: #969696;">手机号<span class="tel"></span></h6>
				<div class="log contant"><label id="login4_label" style="visibility: hidden;">登录密码</label>
					<input type="password" placeholder="6-18位登录密码" id="enter_pwd" maxlength="16" />
					<a class="btn" disabled="true" id="d" style="opacity: 0.36;pointer-events:none;" onclick="return fun(this,4)">登录</a>
				</div>
				<h6 style="padding-left: 1.8em;margin-top: -0.8rem;color: #FA7C25;">忘记密码?</h6>
			</section>
		</section>
	</body>

</html>
<script src="js/login.js"></script>
