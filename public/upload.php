<?php 

	class upload
	{
		public function __construct()
		{
			// echo '111';die;

		}

		// 判断文件是否上传
		public function is_upload()
		{
			$key = key($_FILES);  // file表单的name名
				if( empty($key) ){
					return 1;
			}

			$error = $_FILES[$key]['error'];
				if($error == 4 ){
					return 2;
				}

				return 3;

		}

		// 单文件上传
		public function singleFile($savePath = '../upload/', $allowType = array('image'))
		{
			// 1. 判断错误号
				// post大小 8M
				$key = key($_FILES);  // file表单的name名
				if( empty($key) ){
					return '您的文件太大, 请换个小点的';
				}

				$error = $_FILES[$key]['error'];
				if($error > 0){
					switch ($error) {
						case '1': return '您的文件太大, 请换个小点的'; break;
						case '2': return '您的文件太大, 请换个小点的'; break;
						case '3': return '请检查您的网络'; break;
						case '4': return '没有上传文件'; break;
						case '6': return '服务器出错'; break;
						case '7': return '服务器出错'; break;
					}
				}

			// 2. 判断是否从POST协议上传
				$tmp = $_FILES[$key]['tmp_name'];
				if( !is_uploaded_file($tmp) ){
					return '非法上传';
				}

			// 3. 判断文件类型
				// 3.1 获取文件类型
				$type = strtok($_FILES[$key]['type'], '/');
				// 3.2 判断$type是否在允许类型中
				if( !in_array($type, $allowType) ){
					return '类型不符';
				}
				
			// 4. 设置新的文件名
				// 4.1 获取源文件的扩展名
					$suffix = strrchr($_FILES[$key]['name'], '.');
				// 4.2 设置新的 文件名
					$fileName = date('Ymd').uniqid().$suffix;

			// 5. 设置新的存储目录
					$savePath .= date('/Y/m/d/');
					// 5.2 判断存储目录是否存在
					if( !file_exists($savePath) ){
						mkdir($savePath, 0777, true);
					}

			// 6. 移动文件
					if( move_uploaded_file( $tmp , $savePath.$fileName )){
						$arr[] = $fileName;
						return $arr; 
					}
					return '上传失败';
		}

	}
