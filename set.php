<?php
$title="账号设置";
include_once"head.php";
include_once "up.php";

Session_start();
include_once "pdoclass.php";
if(@$_SESSION['uid']==""){
	echo "<script>window.location.href='login.php'</script>";die;
}
include_once "lang.php";

$id = $_SESSION['uid'];
$strSql = "select * from `c_user` where `uid` = '".$id."'";
 // var_dump($strSql);die;
$res = $pdo -> query($strSql, $queryMode = 'Row', $debug = false);
// var_dump($res['icon']);die;
?>
	<style type="text/css">
            #preview {
                display: inline-block;
                width: 2.56rem;
                height: 2.56rem;
                position: relative;
                background-image: url('<?php echo $res['icon'] ?>');
                background-repeat: no-repeat;
                background-size: cover;
            }
            
            #file {
                width: 100%;
                height: 100%;
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
                cursor: pointer;
                z-index: 5;
            }
        </style>
	<body class="panel">
		<section class="main animated fadeIn">
			<section class="title block animated fadeInDown">
				<a onclick="history.back(-1);" class="tl fl ta-black" href="users.php"></a>
				<h1>账号设置</h1>
				<a class="fr tr">编辑</a>
			</section>
			<p class="bghui" style="padding:.1rem .2rem; color: #A3A3A3;"></p>
			<section class="contant users anthen">
				<li>实名认证
					<font class="hui fr"><?php echo $res['name'] ?>
						<span style="border:1px solid #FB883C; color: #FB883C; padding:3px; border-radius: 5px; font-size: .2rem; margin: 0 .1rem;">
						<?php if ($res['audit']==0): ?>
							<a href="xgauthentication.php">未认证</span><img src="images/2018-08-17_104420.png"></a>
						<?php elseif($res['audit']==1) :?>
							正在审核
						<?php elseif($res['audit']==2) :?>
							<a href="authentication.php">已认证</span><img src="images/2018-08-17_104420.png"></a>
						<?php endif ?>
						
					</font>
				</li>							
			</section>
			<p class="bghui" style="padding:.1rem .2rem; color: #A3A3A3;"></p>
			<section class="contant users anthen">
				<a href="#" onclick="$('.tip3').toggle()">
					<li>用户头像
						<font class="hui fr">
							<img style="width: .5rem;" src="<?php echo $res['icon'] ?>"><img src="images/2018-08-17_104420.png">
						</font>
					</li>
				</a>
				<a href="#" onclick="$('.tip1').toggle()">
					<li>用户昵称
						<font class="hui fr"><?php echo $res['nickname'] ?><img src="images/2018-08-17_104420.png"></font>
					</li>
				</a>
				<li>绑定手机
					<font class="hui fr"><?php echo $res['tel'] ?><img src="images/2018-08-17_104420.png"></font>
				</li>
			
				<a href="#" onclick="$('.tip2').toggle()">
					<li>用户性别
						<font class="hui fr"><?php echo $res['sex']==1?"男":"女" ?><img src="images/2018-08-17_104420.png"></font>
					</li>
				</a>				
			</section>
			<p class="bghui" style="padding:.1rem .2rem; color: #A3A3A3;"></p>
			<section class="contant users anthen">
			<li routeUrl="address.php">我的收货地址
					<font class="hui fr"><img src="images/2018-08-17_104420.png"></font>
				</li>
				<li routeUrl="yinhangka.php">绑定银行卡
					<font class="hui fr"><img src="images/2018-08-17_104420.png"></font>
				</li>
				<li routeUrl="login_m.php">登录密码
					<font class="hui fr"><img src="images/2018-08-17_104420.png"></font>
				</li>
				<li routeUrl="zhifu_m.php">支付密码
					<font class="hui fr"><img src="images/2018-08-17_104420.png"></font>
				</li>
				<li routeUrl="login3.php?out=out&tel=<?php echo $res['tel']?>" class="tc btn" style="line-height:.4rem; margin-top:.5rem"> 退出登录</li>
			</section>
			<form action="sex.php" method="post">
			<section class="tip tip2 animated fadeIn">
			<div class="tip-center center">
				<div class="tip-main animated zoomIn">
					<div class="block" style="text-align: center; font-size: .3rem;">
						<h2 style="padding-top: .2rem;padding-left: .2rem; font-size: .4rem; text-align: left;">修改性别</h2><br />
						<p style="text-align: left; padding: 0 .6rem;">男<input class="fr" style="margin: 0 .2rem;" type="radio" name="sex" value="1"></p>
						<p style="text-align: left; padding: 0 .6rem;">女<input class="fr" style="margin: 0 .2rem;" type="radio" name="sex" value="2"></p>
						<p style="text-align: right; padding-right: .2rem; margin-bottom: .3rem;">
							<a onclick="$('.tip2').toggle()" href="#" class="orange">取消</a>
							<input type="submit" onclick="$('.tip2').toggle()" value="确认" class="orange" style="margin-left: 20%">
						</p>
					</div>
				</div>
			</div>
			</section>
			</form>
			<form action="nicheng.php" method="post">
			<section class="tip tip1 animated fadeIn">
			<div class="tip-center center">
				<div class="tip-main animated zoomIn">
					<div class="block" style="text-align: center; font-size: .3rem;">
						<h2 style="padding-top: .2rem;padding-left: .2rem; font-size: .4rem; text-align: left;">修改昵称</h2><br />
						原昵称：<input type="text" value="<?php echo $res['nickname'] ?>" disabled="disabled" style="color: #989898"><br><br>
						新昵称：<input type="text" name="nickname" placeholder="请填写新昵称" style="color: #989898"><br><br>
						<p style="text-align: right; padding-right: .2rem; margin-bottom: .3rem;">
							<a onclick="$('.tip1').toggle()" href="#" class="orange">取消</a>
							<input type="submit" onclick="$('.tip1').toggle()" value="确认修改" class="orange" style="margin-left: 20%">
						</p>
					</div>
				</div>
			</div>
			</section>
			</form>
			<form action="touxiang.php" method="post" enctype="multipart/form-data">
			<section class="tip tip3 animated fadeIn">
			<div class="tip-center center">
				<div class="tip-main animated zoomIn">
					<div class="block" style="text-align: center; font-size: .3rem;">
						<h2 style="padding-top: .2rem;padding-left: .2rem; font-size: .4rem; text-align: left;">点击更换头像</h2><br />
						<div id="preview">
							<input type="file" accept="image/*" id="file" value="" name="r_image">
        				</div>
						<p style="text-align: right; padding-right: .2rem; margin-bottom: .3rem;">
							<a onclick="$('.tip3').toggle()" href="#" class="orange">取消</a>
							<input type="submit" onclick="$('.tip3').toggle()" value="确认修改" class="orange" style="margin-left: 20%">
						</p>
					</div>
				</div>
			</div>
			</section>
			</form>
		</section>		
	</body>

</html>
<script type="text/javascript">
	function fileput(name) {
			$("input[name='" + name + "']").click();
		}
	// 跳转链接
      $(".panel").on("click", "*[routeUrl]", function(e){
        var routeurl = $(this).attr('routeUrl');
        if(!routeurl) return;
        window.location.href = routeurl;
      });

    //头像预览
    var preview = document.querySelector('#preview');
            var eleFile = document.querySelector('#file');
            eleFile.addEventListener('change', function() {
                var file = this.files[0];                
                // 确认选择的文件是图片                
                if(file.type.indexOf("image") == 0) {
                    var reader = new FileReader();
                    reader.readAsDataURL(file);                    
                    reader.onload = function(e) {
                        // 图片base64化
                        var newUrl = this.result;
                        preview.style.backgroundImage = 'url(' + newUrl + ')';
                    };
                }
            });  
</script>
<?php
include_once "js.html";
include_once "admin/autoplan.php";
?>