<?php
$title="添加银行卡";
include_once"head.php";
Session_start();
include_once "pdoclass.php";
if(@$_SESSION['uid']==""){
	echo "<script>window.location.href='login.php'</script>";die;
}
include_once "lang.php";
$uid = $_SESSION['uid'];
//用户
$strSql1 = 'select * from c_user where uid ='.$uid;
$res = $pdo -> query($strSql1, $queryMode = 'Row', $debug = false);
//判断是否已经实名
if ($res['yhk'] == '' || $res['audit'] != '2') {
	zidong('xgauthentication.php','请先完成实名认证');die;
}
if (@$_POST['sub']) {
	// var_dump($_POST);die;
	$by = $_POST['by'];
	function trimall($str)
	{
	    $qian=array(" ","　","\t","\n","\r");
	    $hou=array("","","","","");
	    return str_replace($qian,$hou,$str); 
	}
	$zhihang =$_POST['zhihang'];
	$name = trimall($_POST['name']);
	if ($by == '未选择') {
		zidong('tjyinhangka.php','开户行不能为空');die;
	}
	if (empty($zhihang)) {
		zidong('tjyinhangka.php','银行支行不能为空');die;
	}
	if (empty($name)) {
		zidong('tjyinhangka.php','银行卡不能为空');die;
	}

	$arr = array('u_id'=>$uid,'name'=>$name,'by'=>$by,'zhihang'=>$zhihang);
	$result = $pdo -> insert('w_yhk', $arr, $debug = false);
	zidong('yinhangka.php','添加成功');die;
}
?>

	<body>
		<style>
			.actives{ background-color:#fff; color: #FA7C25;}
			.log {  padding: .4rem;}
			.yhk p {color: #fff;position: absolute;top: .2rem;left: .4rem;font-size: .3rem;}
			.yhk li {position: absolute; top: 1rem; left: .5rem; color: #fff;  font-size: .4rem;}
			.yhk font {position: absolute; top: 2rem; left: .5rem; color: #fff;  font-size: .3rem;}
		</style>
		<section class="main animated fadeIn">
			<section class="title block animated fadeInDown" style=" position: fixed;left: 0;top: 0; z-index: 99; background-color: #fff;">
				<a href="set.php" class="fl ta-black"></a>
				<h1>填写银行卡信息</h1>
				<a class="fr tr "></a>
			</section>
			<p style="padding: .5rem;"></p>
			<section class="contant">
				<div class="yhk gsyh" style="background: linear-gradient(to right, #FA7624, #F6B132);">
					<p><img style="width: .6rem; vertical-align: middle;" src="images/yhk1.png"> <span id="shi" style="color:#fff;">XXXX银行</span></p>
					<li id="shii">XXXX XXXX XXXX XXXX</li>
					<font>持卡人：<?php echo $res['name']?></font>
				</div>				
			</section>
		<form action="" method="post">
			<section class="login contant">
			<div class="log contant"><label>开户行</label>
				<select name="by" id="bank" style="margin-left:.2rem;text-align:center; border-bottom: 1px solid #FC6700;">
					<option>未选择</option>
					<option value="中国银行">中国银行</option>
					<option value="中国建设银行">中国建设银行</option>
					<option value="中国农业银行">中国农业银行</option>
					<option value="中国工商银行">中国工商银行</option>
				</select>
				</div>
				<div class="log contant"><label>开户行支行</label>
					<input type="text" placeholder="请输入开户行支行" name="zhihang"/>
				</div>
				<div class="log contant"><label>银行卡号</label>
					<input type="text" placeholder="请输入银行卡号"  name="name"  id="name" maxlength="23"/>
	
				</div>
				
			</section>
			<section class="bar block animated fadeInUp" style="padding: 0; position: fixed;left: 0; bottom: 0;">
				<input type="submit" style="border-radius: 0;width: 100%; margin: 0;height: 0.8rem; line-height: 0.8rem;" class="btn fr" value="添加" name="sub">
			</section>
		</form>
		</section>
	</body>

</html>
<script>
  $("#bank").bind('input propertychange', function() { 
 	 var val=$(this).val();
 	 $("#shi").html(val);
});;
  $("#name").bind('input propertychange', function() { 
 	 var val=$(this).val();
 	 $("#shii").html(val);
});;
  !function () {
		  document.getElementById('name').onkeyup = function (event) {
		    var v = this.value;
		    if(/\S{5}/.test(v)){
		      this.value = v.replace(/\s/g, '').replace(/(.{4})/g, "$1 ");
		    }
		  };
		}();
</script>