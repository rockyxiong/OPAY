<?php 

@Session_start();
$title="投诉意见";
include_once "head.php";
include_once "pdoclass.php";
if(@$_SESSION['uid']==""){
	echo "<script>window.location.href='login.php'</script>";die;
}

include_once "lang.php";
if(@$_POST['sub']){
	//$id = $_SESSION['uid'];
	$arr=$_POST;
	$arr['uid']=$_SESSION['uid'];
	$arr['ts_time']=time();
	function up($file)
    {
        $tmp = $file['tmp_name'];
        $filepath =  'upload/'.date('ymdhis',time()).rand(10,90).'.png';
        if(move_uploaded_file($tmp,$filepath)){
            return $filepath;
        }else{
            return false;
        }
    }
    $arr['pic1']=up($_FILES['pic1'])??'';
    $arr['pic2']=up($_FILES['pic2'])??'';
    /*if (!$arr['pic1'] || !$arr['pic2']) {
		zidong('tousu.php','图片上传失败');die;
    }*/
    unset($arr['sub']);
 	$result = $pdo -> insert('c_complain', $arr, $debug = false);
	zidong('shensu.php','您的意见已被反馈，等待处理...');die;
}
 ?>
	<body>
		<style>
			input{font-size: .3rem;}
			textarea{width: 100%; height: 3rem;background-color: #fff;font-size: .3rem; padding: .2rem;}
		    .p1{font-size: .4rem;}
		    .tup{width: 46%; margin: 2%;border-radius:5px; padding: .2rem;float: left;background-color: #F9F9F9;color: #989898; font-size: .3rem;text-align: center;}
		    .tup img{width: 40%; margin: .2rem 0; height: 1rem;}
		</style>
		<form action="tousu.php" method="post" enctype="multipart/form-data">
		<section class="main animated bghui fadeIn">
			<section class="title block animated fadeInDown" style="background-color: #fff;">
				<a href="shensu.php" class="fl ta-black"></a>
				<h1>提交申诉</h1>
				<a class="fr tr"></a>
			</section>
			<p style="padding: .1rem .2rem;" class="hui">标题</p>
			<section class="contant" style="background-color: #fff;">
				<input type="text" placeholder="请输入..." name="title">
			</section>
			<p style="padding: .1rem .2rem;" class="hui">类型</p>
			<section class="contant" style="background-color: #fff;font-size: .3rem;">
				申诉类型<a onclick="$('.tip').toggle()" class="fr"><font id="qxz" class="orange">请选择</font><img style="width: .3rem;margin-left:.3rem;" src="images/2018-08-25_100559.png"></a>
			</section>
			<p style="padding: .1rem .2rem;" class="hui">凭证(上传照片必须真实清晰才能通过审核)</p>
			<section class="contant" style="background-color: #fff;font-size: .3rem;">
				<div class="tup">
					<input style="display: none;" class="dida" onchange="imgyulan(this)" type="file" value="" name="pic1">	
				    <img onclick="return $('.dida').click();" onerror="this.src='images/2018-08-25_095908.png'" src=""><br>上传凭证1
				</div>
				<div class="tup">
					<input style="display: none;" class="dida1" onchange="imgyulan(this)" type="file"  value="" name="pic2">	
				    <img onclick="return $('.dida1').click();" onerror="this.src='images/2018-08-25_095908.png'" src=""><br>上传凭证2
				</div>
			</section>
			<p style="padding: .1rem .2rem;" class="hui">留言</p>
			<section class="" >
				<textarea placeholder="请输入..." name="con"></textarea>
			</section>
			<input type="submit" name="sub" value="提交" class="btn">
			
		</section>
		<section class="tip animated fadeIn">
			<div class="tip-center center">
				<div class="tip-main animated zoomIn">
					<div class="block" style="text-align: center; font-size: .3rem;">
						<h2>提示</h2><br />
						<p class="p1">投诉<input type="radio" name="type"style="margin-right: .4rem;width: .5rem; margin-top: .3rem;" class="fr zz" value="投诉" checked></p>
						<p class="p1">建议<input type="radio" name="type" style="margin-right: .4rem;width: .5rem; margin-top: .3rem;" class="fr zz" value="建议"></p>
						<p class="p1">其他<input type="radio" name="type" style="margin-right: .4rem;width: .5rem; margin-top: .3rem;" class="fr zz" value="其他"></p>
						<p class="p2">
							<a onclick="quxiao()">取消</a>
							<a  onclick="queding()">确认</a>
						</p>
					</div>
				</div>
			</div>

		</section>
		</form>
	</body>
	<script>
		function imgyulan(obj) {
			var $file = $(obj);
			var objUrl = $file[0].files[0];
			//获得一个http格式的url路径:mozilla(firefox)||webkit or chrome  
			var windowURL = window.URL || window.webkitURL;
			//createObjectURL创建一个指向该参数对象(图片)的URL  
			var dataURL = windowURL.createObjectURL(objUrl);
			$file.next().attr("src", dataURL);
		}	
		function quxiao(){
			$('.tip').toggle();
			$("#qxz").html('请选择...');
		}

		function queding(){
			$('.tip').toggle();
			var tt=$('input:radio[name="type"]:checked').val();
			$("#qxz").html(tt);
			
		}	
	</script>

</html>