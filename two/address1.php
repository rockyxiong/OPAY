﻿<!DOCTYPE html>
<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<link href="styles/addres.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="styles/ume.css">
		<link rel="stylesheet" href="styles/animate.css">
		<link rel="stylesheet" href="styles/styles.css">
		<script src="js/jquery.min.js"></script>
		<script src="js/ume.js"></script>
	</head>

	<body>
		<style type="text/css">
			li {
				
			}
		</style>
		<section class="main animated fadeIn">
			<section class="title block animated fadeInDown">
				<a href="shangcheng.php" class="fl ta-black"></a>
				<h1 style="text-align: center; width: 70%;">当前城市—<font id="citys">上海市</font></h1>
				<a class="fr"></a>
			</section>
			<!--显示点击的是哪一个字母-->
			<div id="showLetter" class="showLetter"><span>A</span></div>
			<!--城市索引查询-->
			<div class="letter" style="margin-top: 2.1rem; z-index: 99;">
				<ul>
					<li>
						<a href="javascript:;">热门</a>
					</li>
					<li>
						<a href="javascript:;">A</a>
					</li>
					<li>
						<a href="javascript:;">B</a>
					</li>
					<li>
						<a href="javascript:;">C</a>
					</li>
					<li>
						<a href="javascript:;">D</a>
					</li>
					<li>
						<a href="javascript:;">E</a>
					</li>
					<li>
						<a href="javascript:;">F</a>
					</li>
					<li>
						<a href="javascript:;">G</a>
					</li>
					<li>
						<a href="javascript:;">H</a>
					</li>
					<li>
						<a href="javascript:;">J</a>
					</li>
					<li>
						<a href="javascript:;">K</a>
					</li>
					<li>
						<a href="javascript:;">L</a>
					</li>
					<li>
						<a href="javascript:;">M</a>
					</li>
					<li>
						<a href="javascript:;">N</a>
					</li>
					<li>
						<a href="javascript:;">P</a>
					</li>
					<li>
						<a href="javascript:;">Q</a>
					</li>
					<li>
						<a href="javascript:;">R</a>
					</li>
					<li>
						<a href="javascript:;">S</a>
					</li>
					<li>
						<a href="javascript:;">T</a>
					</li>
					<li>
						<a href="javascript:;">W</a>
					</li>
					<li>
						<a href="javascript:;">X</a>
					</li>
					<li>
						<a href="javascript:;">Y</a>
					</li>
					<li>
						<a href="javascript:;">Z</a>
					</li>
				</ul>
			</div>
			<!--城市列表-->
			<div style="padding: .2rem; background-color: #E8ECF1;">
				<input type="text" style="border-radius: 5px; border: none; width: 100%; text-align: center; font-size: .2rem; padding:.1rem ; " placeholder="请输入城市名称">
			</div>
			<div class="container">

				<div class="city">

				</div>
			</div>
		</section>
		<script type="text/javascript" src="./js/jquery.min.js"></script>
		<script type="text/javascript" src="./js/zepto.js"></script>
		<script type="text/javascript" src="./js/city.js"></script>

	</body>

</html>