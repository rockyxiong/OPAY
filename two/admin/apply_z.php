<?php
include_once 'is_login.php';
$title="申请列表";
include_once 'top.php';
if(@$name=$_GET['like']){
    $strSql = "select * from c_user where name like '%".$name."%' or tel='".$name."' or nickname like '%".$name."%' where status=1";
}else{
    $strSql ="select a.money,a.status,a.ddh,a.way,a.u_id,a.time,a.id,u.uid,u.name from c_apply a join c_user u on a.u_id=u.uid where status=1";
}

$arr = $pdo -> query($strSql, $queryMode = 'All', $debug = false);

?>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 申请中心 <span class="c-gray en">&gt;</span> 已处理申请 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="pd-20">
    <div class="text-c">
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
    <span class="l"><a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="icon-trash"></i> 批量删除</a>
    </div>
    <table class="table table-border table-bordered table-hover table-bg table-sort">
        <thead>
        <tr class="text-c">
            <th width="25"><input type="checkbox" name="" value=""></th>
            <th width="20">编号</th>
             <th width="20">ID</th>
            <th width="100">姓名</th>
            <th width="40">操作</th>
            <th width="90">金额</th>
            <th width="150">状态</th>
            <th width="80">操作</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i=1;
        foreach($arr as $v){
            ?>
            <tr class="text-c">
                <td><input type="checkbox" value="<?php echo $v['uid']?>" name="subChk"></td>
                <td><?php echo $i++;?></td>
                <td><?php echo $v['ddh'];?></td>
                <td><?php echo $v['name']?></td>
                <td>
                    <?php
                    if($v['way']==1){
                        echo "提现";
                    }else{
                        echo "充值";
                    }
                    ?>
                </td>
                <td><?php echo $v['money']?></td>
                <td>
                    已处理
                </td>
                <td class="td-manage">
                     <a title="处理" href="od.php?id=<?php echo $v['id']?>"  class="ml-5" style="text-decoration:none">处理</a></td> 
            </tr>
        <?php }?>
        </tbody>
    </table>
    <div id="pageNav" class="pageNav"></div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    window.onload = (function(){
        // optional set
        pageNav.pre="&lt;上一页";
        pageNav.next="下一页&gt;";
        // p,当前页码,pn,总页面
        pageNav.fn = function(p,pn){$("#pageinfo").text("当前页:"+p+" 总页: "+pn);};
        //重写分页状态,跳到第三页,总页33页
        pageNav.go(1,13);
    });
    function datadel(){

        if(confirm("确定要删除所选项目？")) {
            var checkedList = new Array();
            $("input[name='subChk']:checked").each(function() {
                checkedList.push($(this).val());
            });
            var b=checkedList.toString();
            $.get('user-list.php',{ids:b},function(data){
                alert("删除成功");
                location.replace(location.href);
            })
        }
    }

</script>
</body>
</html>
