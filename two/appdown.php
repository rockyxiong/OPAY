<?php
include_once "lang.php";

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="window-target" content="_top">
<meta content="telephone=no" name="format-detection">
<title><?php echo $p['appxiazai'] ?></title>
<meta name="Keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<link rel="stylesheet" href="styles/ume.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/styles.css">

<script src="js/jquery.min.js"></script>
<script src="js/ume.js"></script>

</head>
    
<body>
<section id="tip" style="width:100%;height:160px;color:yellow;background:#000;line-height:50px;padding:20px;display:none;">
    <?php echo $p['weixinxiazai'] ?><br><?php echo $p['liulanqidakai'] ?>
</section>
<section class="main animated fadeIn">
	<section class="login contant">
		<div class="logo"><img src="images/logo.svg"></div>
		<div class="login-form contant" style="margin-bottom: 200px;">
			<a href="https://www.pgyer.com/opay" class="btn"><?php echo $p['iosxiazai'] ?></a>
		    <a href="opay.apk?v=1" class="btn"><?php echo $p['androidxiazai'] ?></a>
		</div>
	</section>
	
</section>
<section class="bar block animated fadeInUp">
	<a href="index.php"><i class="bai-1"></i><?php echo $p['qianbao'] ?></a>
<a href="jiaoyi.html"><i class="bai-2"></i><?php echo $p['jiaoyi'] ?></a>
<a href="shangcheng.php"><i class="bai-3"></i><?php echo $p['shangcheng'] ?></a>
<a href="user.php" class="active"><i class="bai-4"></i><?php echo $p['wode'] ?></a>
</section>
<script>
 var ua = window.navigator.userAgent.toLowerCase();
  //通过正则表达式匹配ua中是否含有MicroMessenger字符串
  if(ua.match(/MicroMessenger/i) == 'micromessenger'){
  $('#tip').show();
  }
</script>
</body>
</html>