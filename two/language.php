<?php
include_once "lang.php";

// var_dump($p);die
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?php echo $p['xuanzeyuyan'] ?></title>
		<script type="text/javascript">
			document.addEventListener('plusready', function() {
				//console.log("所有plus api都应该在此事件发生后调用，否则会出现plus is undefined。"

			});
		</script>
		<link rel="stylesheet" href="css/style.css" />
	</head>

	<body>
		<div class="box" style="background-color: #F8F8F8;">
			<div class="header">
				<div class="top_her">
					<a href="javascript:;" onclick="history.back(-1);"><img src="images/fanhui.png"></a><span><?php echo $p['xuanzeyuyan'] ?></span>
				</div>
			</div>
			<div class="login">
				<h1><?php echo $p['xuanzeyuyan'] ?></h1>
			</div>
			<div class="uesr_center">

				<form action="lang.php">
					<ul class="logins">
						<li><label style="width: 5em"  class=""><?php echo $p['xuanzeyuyan'] ?></label>
							<select name="lang" style="width: 200px;height: 40px;border-radius: 5px;box-shadow: 0 0 5px #ccc;" id="lang">
								<option value="ch"><?php echo $p['zhongwen'] ?></option>
								<option value="en"><?php echo $p['yingwen'] ?></option>
								<option value="jp"><?php echo $p['riyu'] ?></option>
								<option value="hy"><?php echo $p['hanyu'] ?></option>
							</select>
						</li>
					</ul>
					<div class="btns"><input type="submit" style="border: none; font-size: 1.2em;" class="btn_t" value="<?php echo $p['querenxiugai'] ?>"></div>
				</form>
			</div>
		
		</div>
	</body>
</html>