<?php

function queding($url,$dizhi){
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="window-target" content="_top">
<meta content="telephone=no" name="format-detection">
<title>温馨提示</title>
<meta name="Keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<link rel="stylesheet" href="styles/ume.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/styles.css">

<script src="js/jquery.min.js"></script>
<script src="js/ume.js"></script>
<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
</head> 
<section class="tip animated fadeIn" style="display:block;z-index:999">
<div class="tip-center center">
	<div class="tip-main animated zoomIn">
		<div class="block" style="text-align: center; font-size: .3rem;">
			<h2 style="padding-top: .2rem;padding-left: .2rem; font-size: .4rem; text-align: left;">提示</h2><br />
			<p style="text-align: left; padding-left: .2rem;">定位到<?php echo $dizhi?>，是否更换？</p >
			<p style="text-align: right; padding-right: .2rem; margin-bottom: .3rem;"><a href="<?php echo $url?>?t=2" onclick="$('.tip').toggle()"  style="margin: 0 .3rem;  color: #ccc;">取消</a><a href="<?php echo $url?>?t=1" style="color: #f00;">确认</a></p>
		</div>
	</div>
</div>
</section>

<?php }?>