<?php
include_once "lang.php";

// var_dump($p);die
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="window-target" content="_top">
<meta content="telephone=no" name="format-detection">
<title><?php echo $p['wode'] ?></title>
<meta name="Keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<link rel="stylesheet" href="styles/ume.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/styles.css">

<script src="js/jquery.min.js"></script>
<script src="js/ume.js"></script>
<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

</head>
    
<body>
<section class="main animated fadeIn">
	<section class="user contant animated fadeInDown">
		<a href="set.php"><i class="ui-info"></i><?php echo $p['gerenxinxi'] ?></a>
		<a href="run.php"><i class="ui-run"></i><?php echo $p['benpaoshuju'] ?></a>
	</section>
	<section class="list contant" style="font-size:1.5em">
		<a href="tuiguang.php"><li><i class="lli-12"></i><?php echo $p['woyaotuiguang'] ?></li></a>
		<a href="zhongchou.php"><li><i class="lli-13"></i><?php echo $p['zongchou'] ?></li></a>
		<a href="gonggao.php"><li><i class="lli-5"></i><?php echo $p['gonggao'] ?></li></a>
		<a><li><i class="lli-14"></i><?php echo $p['renwu'] ?></li></a>
		<a href="tousu.php"><li><i class="lli-15"></i><?php echo $p['tousu'] ?></li></a>
		<a href="dianpu.php"><li><i class="lli-16"></i><?php echo $p['wodedianpu'] ?></li></a>
		<a href="appdown.php"><li><i class="lli-16"></i><?php echo $p['appxiazai'] ?></li></a>
		<a><li><i class="lli-11"></i><?php echo $p['guanyuwomen'] ?></li></a>
		<a href="setout.php" class="btn"><?php echo $p['tuichudenglu'] ?></a>

	</section>
<section class="bar block animated fadeInUp">
	<a href="index.php" ><i class="bai-1"></i><?php echo $p['qianbao'] ?></a>
	<a href="jiaoyi.html"><i class="bai-2"></i><?php echo $p['jiaoyi'] ?></a>
	<a href="shangcheng.php"><i class="bai-3"></i><?php echo $p['shangcheng'] ?></a>
	<a href="user.php" class="active"><i class="bai-4"></i><?php echo $p['wode'] ?></a>
</section>
</body>
</html>

