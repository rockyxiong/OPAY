<?php
function zidong($url,$m,$i){
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="window-target" content="_top">
<meta content="telephone=no" name="format-detection">
<title>温馨提示</title>
<meta name="Keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<link rel="stylesheet" href="styles/ume.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/styles.css">

<script src="js/jquery.min.js"></script>
<script src="js/ume.js"></script>
<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
</head> 
<body onload="$('.tip').toggle()">
<section class="tip animated fadeIn">
	<div class="tip-center center">
		<div class="tip-main animated zoomIn">
			<div class="tip-title block">
				<h2>温馨提示</h2>
				<input type="hidden" id="url" value="<?php echo $url;?>">
			</div>
			<div class="tip-contant block">
                <?php if($i==1){
                ?>
                    <i class="tci-win"></i>
                <?php }else{?>
                    <i class="tci-error"></i>
                <?php }?>

				<h3><?php echo $m?></h3>
			</div>
		</div>
	</div>
</section>
</body>

<script type='text/javascript'>
$(function(){
	var url=$("#url").val();
	setTimeout("location.href='"+url+"'",3000);
})


</script>

</html>
<?php }?>
