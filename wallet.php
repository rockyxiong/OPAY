<!doctype html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="window-target" content="_top">
		<meta content="telephone=no" name="format-detection">
		<title>钱包</title>
		<meta name="Keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" href="styles/ume.css">
		<link rel="stylesheet" href="styles/animate.css">
		<link rel="stylesheet" href="styles/styles.css">

		<script src="js/jquery.min.js"></script>
		<script src="js/ume.js"></script>
		<!--[if lt IE 9]>
    <script src="js/html5.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

	</head>

	<body>
		<section class="main animated fadeIn">
			<section class="title block animated fadeInDown">
				<a class="fl"><img src="images/wallet_03.png"></a>
				<h1>钱包</h1>
				<a class="fr tr"><img src="images/wallet_06.png"></a>
			</section>
			<section class="contant bghui">
				<div class="wallet">
					<p>我的总资产</p>
					<h2>10000.00</h2>
					<span class="borr">我的余额<br><font>200.00</font></span>
					<span>我的积分<br><font>100.00</font></span>
				</div>
				<li class="way">
					<img src="images/wallet_11.png">
					<span><font>Y公益</font><br>Y公益简介，Y公益简介，Y公益简介，Y公益简介，</span>
					<a href="#" class="btn fr">去了解</a>
				</li>
				<li class="way">
					<img src="images/wallet_14.png">
					<span><font>Y信贷</font><br>Y公益简介，Y公益简介，Y公益简介，Y公益简介，</span>
					<a href="#" class="btn fr">去了解</a>
				</li>
				<li class="way">
					<img src="images/wallet_17.png">
					<span><font>Y信用</font><br>Y公益简介，Y公益简介，Y公益简介，Y公益简介，</span>
					<a href="#" class="btn fr">去了解</a>
				</li>
				
			</section>
		</section>
		<section class="bar block animated fadeInUp">
			<a href="#"><i class="bai-1"></i>钱包
			</a>
			<a href="#"><i class="bai-2"></i>交易
			</a>
			<a href="#"><i class="bai-3"></i>扫码
			</a>
			<a href="#" class="active"><i class="bai-4"></i>商圈
			</a>
			<a href="#"><i class="bai-5"></i>我的
			</a>
		</section>
	</body>

</html>